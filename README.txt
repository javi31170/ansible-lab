--> On Ansible server:
ssh-keygen
ssh-copy-id -i ~/.ssh/id_rsa.pub <user>@<docker-host>
vi /etc/ansible/hosts
...
[ubuntu]
ubuntu0 ansible_ssh_host=<docker-host> ansible_ssh_port=4020 ansible_ssh_user=root
ubuntu1 ansible_ssh_host=<docker-host> ansible_ssh_port=4021 ansible_ssh_user=root
ubuntu2 ansible_ssh_host=<docker-host> ansible_ssh_port=4022 ansible_ssh_user=root
ubuntu3 ansible_ssh_host=<docker-host> ansible_ssh_port=4023 ansible_ssh_user=root
ubuntu4 ansible_ssh_host=<docker-host> ansible_ssh_port=4024 ansible_ssh_user=root
ubuntu5 ansible_ssh_host=<docker-host> ansible_ssh_port=4025 ansible_ssh_user=root
ubuntu6 ansible_ssh_host=<docker-host> ansible_ssh_port=4026 ansible_ssh_user=root
ubuntu7 ansible_ssh_host=<docker-host> ansible_ssh_port=4027 ansible_ssh_user=root
ubuntu8 ansible_ssh_host=<docker-host> ansible_ssh_port=4028 ansible_ssh_user=root
ubuntu9 ansible_ssh_host=<docker-host> ansible_ssh_port=4029 ansible_ssh_user=root

[centos]
centos0 ansible_ssh_host=<docker-host> ansible_ssh_port=5020 ansible_ssh_user=root
centos1 ansible_ssh_host=<docker-host> ansible_ssh_port=5021 ansible_ssh_user=root
centos2 ansible_ssh_host=<docker-host> ansible_ssh_port=5022 ansible_ssh_user=root
centos3 ansible_ssh_host=<docker-host> ansible_ssh_port=5023 ansible_ssh_user=root
centos4 ansible_ssh_host=<docker-host> ansible_ssh_port=5024 ansible_ssh_user=root
centos5 ansible_ssh_host=<docker-host> ansible_ssh_port=5025 ansible_ssh_user=root
centos6 ansible_ssh_host=<docker-host> ansible_ssh_port=5026 ansible_ssh_user=root
centos7 ansible_ssh_host=<docker-host> ansible_ssh_port=5027 ansible_ssh_user=root
centos8 ansible_ssh_host=<docker-host> ansible_ssh_port=5028 ansible_ssh_user=root
centos9 ansible_ssh_host=<docker-host> ansible_ssh_port=5029 ansible_ssh_user=root

[debian]
debian0 ansible_ssh_host=<docker-host> ansible_ssh_port=6020 ansible_ssh_user=root
debian1 ansible_ssh_host=<docker-host> ansible_ssh_port=6021 ansible_ssh_user=root
debian2 ansible_ssh_host=<docker-host> ansible_ssh_port=6022 ansible_ssh_user=root
debian3 ansible_ssh_host=<docker-host> ansible_ssh_port=6023 ansible_ssh_user=root
debian4 ansible_ssh_host=<docker-host> ansible_ssh_port=6024 ansible_ssh_user=root
debian5 ansible_ssh_host=<docker-host> ansible_ssh_port=6025 ansible_ssh_user=root
debian6 ansible_ssh_host=<docker-host> ansible_ssh_port=6026 ansible_ssh_user=root
debian7 ansible_ssh_host=<docker-host> ansible_ssh_port=6027 ansible_ssh_user=root
debian8 ansible_ssh_host=<docker-host> ansible_ssh_port=6028 ansible_ssh_user=root
debian9 ansible_ssh_host=<docker-host> ansible_ssh_port=6029 ansible_ssh_user=root
...

--> On Docker host:
git clone git@gitlab.com:javi31170/ansible-lab.git
cd ansible-lab
docker build -t centos-ansible-client - < centos_sshd
docker build -t debian-ansible-client - < debian_sshd
docker build -t ubuntu-ansible-client - < ubuntu_sshd
chmod +x ./containers.sh
./containers.sh crec (create CentOS containers)
./containers.sh status
./containers.sh delc (delete CentOS containers)

--> On Ansible server:
git clone git@gitlab.com:javi31170/ansible-lab.git
cd ansible-lab
ansible -m ping centos
ansible-playbook nginx_c.yml