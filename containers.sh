#!/bin/bash
#
#RANGE=$(echo {00..09})
RANGE=$(echo {0..9})

case "$1" in
  crea)
    echo "Create Ubuntu Containers"
    for i in $RANGE; \
    do echo $i; \
    docker run --name ubuntu$i --hostname=ubuntu$i -d -p 402$i:22 -p 800$i:80 -p 801$i:443 --restart unless-stopped --volume /var/lib/ubuntu$i ubuntu-ansible-client; \
    done
    for i in $RANGE; do docker cp /root/.ssh/authorized_keys ubuntu$i:/root/.ssh/authorized_keys; done
    echo "----------------------------------"
    echo "Create Centos Containers"
    for i in $RANGE; \
    do echo $i; \
    docker run --name centos$i --hostname=centos$i -d -p 502$i:22 -p 802$i:80 -p 803$i:443 --restart unless-stopped --volume /var/lib/centos$i centos-ansible-client; \
    done
    for i in $RANGE; do docker cp /root/.ssh/authorized_keys centos$i:/root/.ssh/authorized_keys; done
    echo "----------------------------------"
    echo "Create Debian Containers"
    for i in $RANGE; \
    do echo $i; \
    docker run --name debian$i --hostname=debian$i -d -p 602$i:22 -p 805$i:80 -p 806$i:443 --restart unless-stopped --volume /var/lib/debian$i debian-ansible-client; \
    done
    for i in $RANGE; do docker cp /root/.ssh/authorized_keys debian$i:/root/.ssh/authorized_keys; done
    echo "----------------------------------"
    docker ps -a
    ;;
  creu)
    echo "Create Ubuntu Containers"
    for i in $RANGE; \
    do echo $i; \
    docker run --name ubuntu$i --hostname=ubuntu$i -d -p 402$i:22 -p 800$i:80 -p 801$i:443 --restart unless-stopped --volume /var/lib/ubuntu$i ubuntu-ansible-client; \
    done
    for i in $RANGE; do docker cp /root/.ssh/authorized_keys ubuntu$i:/root/.ssh/authorized_keys; done
    echo "----------------------------------"
    docker ps -a
    ;;
  crec)
    echo "Create Centos Containers"
    for i in $RANGE; \
    do echo $i; \
    docker run --name centos$i --hostname=centos$i -d -p 502$i:22 -p 802$i:80 -p 803$i:443 --restart unless-stopped --volume /var/lib/centos$i centos-ansible-client; \
    done
    for i in $RANGE; do docker cp /root/.ssh/authorized_keys centos$i:/root/.ssh/authorized_keys; done
    echo "----------------------------------"
    docker ps -a
    ;;
  cred)
    echo "Create Debian Containers"
    for i in $RANGE; \
    do echo $i; \
    docker run --name debian$i --hostname=debian$i -d -p 602$i:22 -p 805$i:80 -p 806$i:443 --restart unless-stopped --volume /var/lib/debian$i debian-ansible-client; \
    done
    for i in $RANGE; do docker cp /root/.ssh/authorized_keys debian$i:/root/.ssh/authorized_keys; done
    echo "----------------------------------"
    docker ps -a
    ;;
  delu)
    for i in $(docker ps -a|grep ubuntu|awk '{print $1}'); do docker stop $i; docker rm $i; done
    echo "----------------------------------"
    docker ps -a
    docker volume prune -f
    ;;
  deld)
    for i in $(docker ps -a|grep debian|awk '{print $1}'); do docker stop $i; docker rm $i; done
    echo "----------------------------------"
    docker ps -a
    docker volume prune -f
    ;;
  delc)
    for i in $(docker ps -a|grep centos|awk '{print $1}'); do docker stop $i; docker rm $i; done
    echo "----------------------------------"
    docker volume prune -f
    docker ps -a
    ;;
  dela)
    for i in $(docker ps -a|grep ubuntu|awk '{print $1}'); do docker stop $i; docker rm $i; done
    for i in $(docker ps -a|grep centos|awk '{print $1}'); do docker stop $i; docker rm $i; done
    for i in $(docker ps -a|grep debian|awk '{print $1}'); do docker stop $i; docker rm $i; done
    #docker rm $(docker ps -a -q)
    docker volume prune -f
    echo "----------------------------------"
    docker ps -a
    ;;
  status)
    docker ps -a
    echo "----------------------------------"
    docker images
    ;;
  *)
    echo "Usage: containers.sh {cred|crea|creu|delu|dela|crec|delc|deld|status}"
    exit 1
    ;;
esac

exit 0